#include <boost/program_options.hpp>
#include <cstdlib>
#include <iostream>
#include <optional>
#include <thread>
#include <crow_all.h>
#include "mkseg.hpp"

namespace po = boost::program_options;

po::variables_map parse_args(int& argc, char* argv[]) {
  // Initialize the default port with the value from the "PORT" environment
  // variable or with 8080.
  auto port = [&]() -> std::uint16_t {
    auto env = std::getenv("PORT");
    if (env == nullptr) return 8080;
    auto value = std::stoi(env);
    if (value < std::numeric_limits<std::uint16_t>::min() ||
        value > std::numeric_limits<std::uint16_t>::max()) {
      std::ostringstream os;
      os << "The PORT environment variable value (" << value
         << ") is out of range.";
      throw std::invalid_argument(std::move(os).str());
    }
    return static_cast<std::uint16_t>(value);
  }();

  // Parse the command-line options.
  po::options_description desc("Server configuration");
  desc.add_options()
      //
      ("help", "produce help message")
      //
      ("address", po::value<std::string>()->default_value("0.0.0.0"),
       "set listening address")
      //
      ("port", po::value<std::uint16_t>()->default_value(port),
       "set listening port");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  if (vm.count("help")) {
    std::cout << desc << "\n";
  }
  return vm;
}

template <typename T, typename K1, typename K2>
double KL_Divergence(K1 count1, K2 count2, std::unordered_map<T, K1> a, std::unordered_map<T, K2> b) {
  double ret = 0;

  for (const auto &[k, v] : a) {
    if (b.count(k))
      ret += 1.0l * v / count1 * (log(1.0l * v / count1) - log(1.0l * b[k] / count2));
  }

  return ret;
}

crow::json::wvalue process(std::string doc1, std::string doc2) {
  auto seg1_list = mkseg_do(doc1);
  auto seg2_list = mkseg_do(doc2);

  std::vector<std::string> seg1(seg1_list.begin(), seg1_list.end());
  std::vector<std::string> seg2(seg2_list.begin(), seg2_list.end());

  std::unordered_map<std::string, size_t> freq1, freq2;
  std::unordered_map<std::string, double> m;

  size_t count1 = 0, count2 = 0, same_count = 0;
  double count_m = 0;

  for (const auto &s : seg1) {
    if (!is_stopword(s)) {
      freq1[s]++;
      count1++;
      count_m += 0.5l;
      m[s] += 0.5l;
    }
  }

  for (const auto &s : seg2) {
    if (!is_stopword(s)) {
      freq2[s]++;
      count2++;
      count_m += 0.5l;
      m[s] += 0.5l;
    }
  }

  for (const auto &[k, v] : freq1) {
    if (freq2.count(k))
      same_count += std::min(v, freq2[k]);
  }

  double js_divergence = 0.5 * (KL_Divergence(count1, count_m, freq1, m) + KL_Divergence(count2, count_m, freq2, m));

  crow::json::wvalue ret;
  ret["seg1"] = seg1;
  ret["seg2"] = seg2;

  for (const auto &[k, v] : freq1) {
    ret["freq1"][k] = v;
  }

  for (const auto &[k, v] : freq2) {
    ret["freq2"][k] = v;
  }

  ret["js"] = js_divergence;
  ret["count1"] = count1;
  ret["count2"] = count2;
  ret["same_count"] = same_count;

  return ret;
}

struct CORSMiddleware {
  template<typename ... Middlewares>
  void init(crow::Crow<Middlewares...>& app) { }

  struct context {};

  void before_handle(crow::request& req, crow::response& res, context& ctx) {
    if (req.method == "OPTIONS"_method) {
      res.set_header("Access-Control-Allow-Credentials", "true");
      res.set_header("Access-Control-Allow-Origin", "*");
      res.set_header("Access-Control-Allow-Methods", "*");
      res.set_header("Access-Control-Allow-Headers", "*");
      res.end();
      return;
    }
  }

  void after_handle(crow::request&, crow::response& res, context& ctx) {
    res.set_header("Access-Control-Allow-Credentials", "true");
    res.set_header("Access-Control-Allow-Origin", "*");
    res.set_header("Access-Control-Allow-Methods", "*");
    res.set_header("Access-Control-Allow-Headers", "*");
  }
};

int main(int argc, char* argv[]) {
  po::variables_map vm = parse_args(argc, argv);

  if (vm.count("help")) return 0;

  auto address = vm["address"].as<std::string>();
  auto port = vm["port"].as<std::uint16_t>();
  std::cout << "Listening on " << address << ":" << port << std::endl;

  crow::Crow<CORSMiddleware> app;
  // crow::SimpleApp app;

  CROW_ROUTE(app, "/")
  .methods("POST"_method)([](const crow::request& req) {
    auto data = crow::json::load(req.body);

    if (!data) {
      return crow::response(500);
    }

    auto doc1 = data["doc1"].s();
    auto doc2 = data["doc2"].s();

    crow::json::wvalue ret = process(doc1, doc2);
    return crow::response(ret);
  });

  app.port(port).bindaddr(address).multithreaded().run();
}
