#pragma once

extern "C" {
  extern char _binary_dict_bin_start[];
  extern char _binary_bf_bin_start[];
  extern char _binary_dict_bin_end[];
  extern char _binary_bf_bin_end[];
}
