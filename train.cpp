#include <iostream>
#include <fstream>
#include <string>
#include <string_view>
#include <vector>
#include <memory>
#include <chrono>
#include <bitset>
#include <unordered_map>
#include <boost/locale.hpp>
#include "rabin_karp.hpp"

void load_dictionary(char *path, std::unordered_map<std::string, size_t> &freq) {
  std::cout << "Loading dictionary" << std::endl;
  std::string s;

  freq[""] = 1;

  std::ifstream fin1(path, std::ios::in);
  while (fin1 >> s) {
    freq[s] = 1;
  }
  fin1.close();
}

size_t load_corpus(char *path, std::unordered_map<std::string, size_t> &freq) {
  std::cout << "Loading corpus" << std::endl;
  std::string s;
  size_t count = 0;

  std::ifstream fin2(path, std::ios::in);
  while (fin2 >> s) {
    if (freq.find(s) != freq.end()) {
      freq[s]++;
      count++;
    }
  }
  fin2.close();

  return count;
}

int main(int argc, char* argv[]) {
  if (argc < 3) {
    std::cerr << "USAGE: train dictionary.txt corpus.txt" << std::endl;
    return -1;
  }

  std::unordered_map<std::string, size_t> freq;
  load_dictionary(argv[1], freq);
  
  size_t count = load_corpus(argv[2], freq);

  generate_sst(count, freq);

  return 0;
}
