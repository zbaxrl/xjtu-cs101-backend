#pragma once
#include <unordered_map>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string_view>
#include <vector>
#include <algorithm>
#include <cmath>

struct hash {
  size_t operator ()(const std::string_view &s) {
    size_t ret = static_cast<size_t>(19260817UL);
    for (char c : s) {
      ret += c * 131;
    }
    return ret;
  }
};

hash string_hash;

template <size_t _Size = 1048576>
struct bloom_filter {
  uint8_t f[(_Size + 7) / 8];

  void clear() {
    memset(f, 0, sizeof(f));
  }

  bool check_str(const std::string_view &s) {
    size_t key = string_hash(s);
    return _check(key);
  }

  void mark_str(const std::string_view &s) {
    size_t key = string_hash(s);
    _mark(key);
  }

  bool _check(size_t pos) {
    pos %= _Size;
    return f[pos / 8] >> (pos & 7);
  }

  void _mark(size_t pos) {
    pos %= _Size;
    f[pos / 8] |= 1 << (pos & 7);
  }
};

struct sst {
  uint32_t length;

  float get(const std::string_view &sv) {
    uint32_t *s_pos = (uint32_t *)(((size_t) this) + sizeof(sst));
    uint32_t *s_freq = (uint32_t *)(((size_t) this) + sizeof(sst) + sizeof(uint32_t) * length);
    char *s_str = (char *)(((size_t) this) + sizeof(sst) + sizeof(uint32_t) * length + sizeof(float) * length);

    std::string s(sv);
    const char *p = s.c_str();

    bool found = false;
    size_t L = 0, R = length; // [L, R)
    while (L < R) {
      int Mid = (L + R) >> 1;
      char *n = s_str + s_pos[Mid];
      int cmp = strcmp(p, n);

      if (cmp < 0)
        R = Mid;
      else if (cmp > 0)
        L = Mid + 1;
      else
        return s_freq[Mid];
    }

    return NAN;
  }
};

void generate_sst(size_t count, std::unordered_map<std::string, size_t> &freq) {
  size_t len = freq.size();
  size_t strlen = 0;

  std::vector<std::pair<std::string, int>> freq_vec(freq.begin(), freq.end());
  sort(freq_vec.begin(), freq_vec.end(), [](const auto &a, const auto &b) {
    return a.first < b.first;
  });

  bloom_filter<> *bf = new bloom_filter<>;
  bf->clear();

  for (auto const& [s, f] : freq_vec) {
    strlen += s.size() + 1;
    bf->mark_str(s);
  }

  size_t sst_size = sizeof(sst) + 2 * sizeof(uint32_t) * len + sizeof(float) * len + sizeof(char) * strlen;
  std::cout << "SST size: " << sst_size / 1048576.0 << " MiB" << std::endl;
  sst *s = (sst*) malloc(sst_size);
  s->length = len;
  uint32_t *s_pos = (uint32_t *)(((size_t) s) + sizeof(sst));
  uint32_t *s_freq = (uint32_t *)(((size_t) s) + sizeof(sst) + sizeof(uint32_t) * len);
  char *s_str = (char *)(((size_t) s) + sizeof(sst) + sizeof(uint32_t) * len + sizeof(float) * len);

  uint32_t pos = 0;
  uint32_t i = 0;
  for (auto const& [str, f] : freq_vec) {
    memcpy(s_str + pos, str.c_str(), str.size() + 1);
    s_freq[i] = std::log(f / count);
    s_pos[i] = pos;
    pos += str.size() + 1;
    i++;
  }

  std::ofstream fout("./dict.bin", std::ios::out | std::ios::binary);
  fout.write((const char *)s, sst_size);
  fout.close();

  std::ofstream fout2("./bf.bin", std::ios::out | std::ios::binary);
  fout2.write((const char *)bf, sizeof(bloom_filter<>));
  fout2.close();
}
