#pragma once
#include <list>
#include <vector>
#include <unordered_set>
#include "dict.h"
#include "rabin_karp.hpp"

bloom_filter<> *bf = reinterpret_cast<bloom_filter<>*>(_binary_bf_bin_start);
sst *st = reinterpret_cast<sst*>(_binary_dict_bin_start);

std::list<std::string_view> mkseg_do(const std::string &s) {
  const float minimum_logprob = -17;
  const float english_word_logprob = -10;
  const float digits_logprob = -5;
  const size_t max_length = 6;

  uint32_t size = 0, pos = 0, ssize = s.size();
  std::vector<uint32_t> cpt;
  while (pos < ssize) {               // utf8 code point decoding
    unsigned char lb = s[pos];
    cpt.push_back(pos);
    if (( lb & 0x80 ) == 0 )          // single byte
      pos += 1;
    else if (( lb & 0xE0 ) == 0xC0 )  // double bytes:    110x xxxx
      pos += 2;
    else if (( lb & 0xF0 ) == 0xE0 )  // triple bytes:    1110 xxxx
      pos += 3;
    else if (( lb & 0xF8 ) == 0xF0 )  // quadruple bytes: 1111 0xxx
      pos += 4;
    else                              // unrecognized, treat as single byte
      pos += 1;
  }
  size = cpt.size();
  cpt.push_back(ssize);

  std::vector<uint32_t> prev(size + 1);
  std::vector<float> log_prob(size + 1);
  std::string_view sv(s);

  for (size_t i = 1; i <= size; i++) {
    log_prob[i] = log_prob[i - 1] + minimum_logprob;
    prev[i] = i - 1;
  }

  pos = 0;
  while (pos < size) {
    if (cpt[pos + 1] - cpt[pos] == 1 && isalpha(sv[cpt[pos]])) {
      size_t start_pos = pos;
      while (pos < size && cpt[pos + 1] - cpt[pos] == 1 && isalpha(sv[cpt[pos]])) pos++;
      if (pos > start_pos) {
        log_prob[pos] = log_prob[start_pos] + english_word_logprob;
        prev[pos] = start_pos;
      }
    }
    pos++;
  }

  pos = 0;
  while (pos < size) {
    if (cpt[pos + 1] - cpt[pos] == 1 && isdigit(sv[cpt[pos]])) {
      size_t start_pos = pos;
      while (pos < size && cpt[pos + 1] - cpt[pos] == 1 && isdigit(sv[cpt[pos]])) pos++;
      if (pos > start_pos) {
        log_prob[pos] = log_prob[start_pos] + english_word_logprob;
        prev[pos] = start_pos;
      }
    }
    pos++;
  }

  for (size_t i = 0; i < size; i++) {
    for (size_t len = std::min(i + 1, max_length); len > 0; len--) {
      std::string_view v = sv.substr(cpt[i - len + 1], cpt[i + 1] - cpt[i - len + 1]);
      if (bf->check_str(v)) {
        float lp = st->get(v);
        if (!std::isnan(lp) && log_prob[i - len + 1] + lp > log_prob[i + 1]) {
          log_prob[i + 1] = log_prob[i - len + 1] + lp;
          prev[i + 1] = i - len + 1;
        }
      }
    }
  }

  std::list<std::string_view> ret;
  int32_t p = size;
  while (p > 0) {
    int32_t pre = prev[p];
    ret.push_front(sv.substr(cpt[pre], cpt[p] - cpt[pre]));
    p = pre;
  }

  return ret;
}

const std::unordered_set<std::string_view> stopword_set = {
  ",", ".", ";", "[", "]", "(", ")", "{", "}", "<", ">", "!", "?", "\"", "'", "/", "\\", "=", "$", "#", "%", "^", "&", "*", "@", "-", "_", "+", "|",
  "，", "。", "（", "）", "！", "【", "】", "《", "》",
  " ", "\n", "\t", "\r", "“", "”", "‘", "’", "？",
  "的", "地", "得", "也",
  "一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "百", "千", "万",
  "人", "以", "何", "因为", "所以", "不但", "而且", "导致", "并且", "因此", "只要", "上", "去", "到",
  "你", "我", "他", "她", "它", "这", "那"
};
bool is_stopword(const std::string_view &sv) {
  return stopword_set.count(sv);
}
